import nox as nox

@nox.session
def lint(session):
    session.install("flake8")
    session.run("flake8", "testabeta001.py")

@nox.session
def tests(session):
    session.install("pytest")
    session.run("pytest")
