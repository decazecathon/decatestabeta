# decatestabeta

Sample project to explore unit testing (Pytest, Tox, Nox, etc) in Python 

This project attempts to describe all the functionality of Pytest with Nox

*\app 
    * All the sample projects
*\tests
    * All the tests 

Added bonus - Gitlab-ci + sonarcloud static analysis 

