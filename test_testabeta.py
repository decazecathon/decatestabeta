import pytest

from testabeta001 import Capital_Case

def test_capital_case():
    assert Capital_Case('sPECTRE') == 'Spectre'